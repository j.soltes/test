<?php


namespace App\Form;

use Symfony\Component\Form\AbstractType;
use App\Entity\MortgageGuide;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class MortgageGuideType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('propertyPrice')
            ->add('amount')
            ->add('repaymentTime')
            ->add('fixation', ChoiceType::class, ['choices' => [
                '1' => 3,
                '3' => 3,
                '5' => 5,
                '10' => 10,
            ]
            ])
            ->add('birthNumber');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MortgageGuide::class,
        ]);
    }
}
