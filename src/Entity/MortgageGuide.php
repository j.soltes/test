<?php

namespace App\Entity;


use Symfony\Component\Validator\Constraints as Assert;


class MortgageGuide
{

    /**
     * @Assert\NotBlank()
     * @Assert\GreaterThan(1000000)
     */
    private $propertyPrice;

    /**
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(1000000)
     */
    private $amount;

    /**
     * @Assert\NotBlank()
     */
    private $birthNumber;

    /**
     * @Assert\NotBlank()
     * @Assert\Range(
     *      min = "1",
     *      max = "10"
     * )
     */
    private $fixation;

    /**
     * @Assert\NotBlank()
     * @Assert\Range(
     *      min = "1",
     *      max = "30"
     * )
     */
    private $repaymentTime;

    public function getPropertyPrice(): ?int
    {
        return $this->propertyPrice;
    }

    public function setPropertyPrice(int $propertyPrice): self
    {
        $this->propertyPrice = $propertyPrice;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getBirthNumber(): ?string
    {
        return $this->birthNumber;
    }

    public function setBirthNumber(string $birthNumber): self
    {
        $this->birthNumber = $birthNumber;

        return $this;
    }

    public function getFixation()
    {
        return $this->fixation;
    }

    public function setFixation($fixation): void
    {
        $this->fixation = $fixation;
    }

    public function getRepaymentTime(): ?int
    {
        return $this->repaymentTime;
    }

    public function setRepaymentTime(int $repaymentTime): self
    {
        $this->repaymentTime = $repaymentTime;

        return $this;
    }
}
