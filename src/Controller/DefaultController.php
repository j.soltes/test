<?php

namespace App\Controller;

use App\Entity\MortgageGuide;
use App\Form\MortgageGuideType;
use App\Utils\TopPojisteni\Helpers;
use App\Utils\TopPojisteni\ProcessXslx;
use App\Utils\TopPojisteni\RestService;
use App\Utils\TopPojisteni\SoapServiceFactory;
use Exception;
use InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;

class DefaultController extends AbstractController
{

    private $soapService;
    private $restService;
    private $processXslx;
    private $mortgage;
    private $filter;

    /**
     * DefaultController constructor.
     */
    public function __construct()
    {
        $this->soapService = SoapServiceFactory::create();
        $this->restService = new RestService('http://www.toppojisteni.net/zadani/rest/');
        $this->processXslx = new ProcessXslx();
        $this->mortgage = new MortgageGuide();
        $this->filter = new Helpers();
    }

    /**
     * @Route("/", name="default")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws Throwable
     */
    public function index(Request $request)
    {
        $soapResult = null;
        $restResult = null;
        $xlsxResult = null;
        $guideForm = $this->createForm(MortgageGuideType::class, $this->mortgage);
        $guideForm->handleRequest($request);

        if ($guideForm->isSubmitted() && $guideForm->isValid()) {
            try {
                $this->restService->addRequestGet('rc.php', ['rc' => $this->mortgage->getBirthNumber()], 'birthNumber');
                $this->restService->addRequestJson('institution.php', [
                    'Amount' => $this->mortgage->getAmount(),
                    'RepTime' => $this->mortgage->getRepaymentTime(),
                ], 'restResult');
                $results = $this->restService->getResults();
                $birthNumberHash = $results['birthNumber']->hash;

                $restResult = $this->filter->filterRestResult($results['restResult'], $this->mortgage->getFixation());

                $soapResult = $this->soapService->getCalc($birthNumberHash,
                    $this->mortgage->getAmount(),
                    $this->mortgage->getPropertyPrice(),
                    $this->mortgage->getRepaymentTime(),
                    $this->mortgage->getFixation());

                $xlsxResult = $this->processXslx->download('http://www.toppojisteni.net/zadani/sazby.xlsx', '.tmp/sazby.xlsx')
                    ->prepareData()
                    ->findResult($this->mortgage->getAmount(),
                        $this->mortgage->getFixation(),
                        $this->mortgage->getRepaymentTime());

            } catch (InvalidArgumentException $e) {
                $this->addFlash('danger', $e->getMessage());
            } catch (Throwable | Exception $e) {
                $this->addFlash('danger', 'We are very sorry but something is broken.');
            }
        }

        return $this->render('default/index.html.twig', [
            'guideForm' => $guideForm->createView(),
            'soapResult' => $soapResult,
            'restResult' => $restResult,
            'xlsxResult' => $xlsxResult,
        ]);
    }
}
