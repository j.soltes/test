<?php


namespace App\Utils;


use Exception;
use InvalidArgumentException;

class FileDownloader
{
    /**
     * @param string $from
     * @param string $to
     * @throws InvalidArgumentException
     * @return bool
     */
    public static function download(string $from, string $to)
    {
        try {
            file_put_contents($to, fopen($from, 'r'));
        } catch (Exception $e) {
            throw new InvalidArgumentException('Can\'t download or create file.');
        }
        return true;
    }
}