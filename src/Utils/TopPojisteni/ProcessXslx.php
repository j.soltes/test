<?php


namespace App\Utils\TopPojisteni;


use App\Utils\FileDownloader;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class ProcessXslx
{
    private $data;
    private $fileDownloader;
    private $tariffXlsxParser;
    private $reader;
    private $file;


    /**
     * ProcessXslx constructor.
     */
    public function __construct()
    {
        $this->fileDownloader = new FileDownloader();
        $this->reader = new Xlsx();
        $this->tariffXlsxParser = new TariffXlsxParser();
        $this->filter = new Helpers();
    }

    public function download($from, $to)
    {
        $this->file = $to;
        $this->fileDownloader->download($from, $this->file);
        return $this;
    }

    /**
     * @return ProcessXslx
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function prepareData()
    {
        $spreadsheet = $this->reader->load($this->file);
        $this->data = $this->tariffXlsxParser->addSpreadsheet($spreadsheet)->setActiveSheet(0)->parseActiveSheet();
        return $this;
    }

    public function findResult(int $mortgageAmount, int $fixation, int $repaymentTime)
    {
        $mortgageAmountKey = $this->filter->findClosestHigherKeys(array_keys($this->data), $mortgageAmount);
        if (array_key_exists($fixation, $this->data[$mortgageAmountKey])) {
            $mortgageRepaymentTimeKey = $this->filter->findClosestHigherKeys(array_keys($this->data[$mortgageAmountKey][$fixation]), $repaymentTime);
        } else {
            return null;
        }

        return $this->data[$mortgageAmountKey][$fixation][$mortgageRepaymentTimeKey];
    }
}