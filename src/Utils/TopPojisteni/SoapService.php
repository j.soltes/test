<?php


namespace App\Utils\TopPojisteni;


use InvalidArgumentException;
use SoapClient;

class SoapService
{
    /**
     * @var SoapClient
     */
    private $soapClient;

    /**
     * SoapService constructor.
     * @param string|null $soapServer
     * @param array|null $params
     */
    public function __construct(?string $soapServer, ?array $params)
    {
        if ($soapServer) {
            $this->soapClient = new SoapClient($soapServer);
        } elseif($params) {
            $this->soapClient = new SoapClient(null, $params);
        }
    }

    /**
     * Get calculated RPSN and interest rate
     * @param string $clientScoringHash birth date hash
     * @param int $amount
     * @param int $houseValue
     * @param int $repaymentTime
     * @param int $fixation
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function getCalc(string $clientScoringHash, int $amount, int $houseValue, int $repaymentTime, int $fixation)
    {
        $this->validateCalcParam($amount, $houseValue, $repaymentTime);

        return $this->soapClient->Calc((object)[
            'clientScoringHash' => $clientScoringHash,
            'amount' => $amount,
            'house_value' => $houseValue,
            'repayment_time' => $repaymentTime,
            'fixation' => $fixation]);
    }

    /**
     * @param int $amount
     */
    private function validateAmount(int $amount): void
    {
        if ($amount < 1000000) {
            throw new InvalidArgumentException('amount < 1000000');
        }
    }

    /**
     * @param int $amount
     * @param int $houseValue
     */
    private function valiadateHouseValue(int $amount, int $houseValue): void
    {
        if ($houseValue <= $amount) {
            throw new InvalidArgumentException('house_value <= amount');
        }
    }

    /**
     * @param int $repaymentTime
     */
    private function validateRepaymentTime(int $repaymentTime): void
    {
        if ($repaymentTime <= 0) {
            throw new InvalidArgumentException('repayment_time <= 0');
        }
    }

    /**
     * @param int $amount
     * @param int $houseValue
     * @param int $repaymentTime
     */
    private function validateCalcParam(int $amount, int $houseValue, int $repaymentTime): void
    {
        $this->validateAmount($amount);
        $this->valiadateHouseValue($amount, $houseValue);
        $this->validateRepaymentTime($repaymentTime);
    }
}