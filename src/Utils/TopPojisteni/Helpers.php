<?php


namespace App\Utils\TopPojisteni;


class Helpers
{

    public function filterRestResult($restResults, int $fixation)
    {
        foreach ($restResults->res as $row) {
            if ($row->fix == $fixation) {
                return (array)$row;
            }
        }
        return null;
    }

    public function findClosestHigherKeys(array $arrayKeys, int $value)
    {
        for ($i = 0; $i < count($arrayKeys); $i++) {
            $key = $arrayKeys[$i];
            if ($arrayKeys[$i] >= $value) {
                break;
            }
        }
        return $key;
    }
}