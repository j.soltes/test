<?php


namespace App\Utils\TopPojisteni;


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class TariffXlsxParser
{
    /**
     * @var Spreadsheet
     */
    private $spreadsheet;
    /**
     * @var Worksheet
     */
    private $sheet;
    private $maxFixationTime = 10;


    public function addSpreadsheet(Spreadsheet $spreadsheet){
        $this->spreadsheet = $spreadsheet;
        return $this;
    }
    /**
     * @param int $index
     * @return TariffXlsxParser
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function setActiveSheet(int $index)
    {
        $this->sheet = $this->spreadsheet->setActiveSheetIndex($index);
        return $this;
    }

    public function parseActiveSheet()
    {
        $i = 0;
        $result = [];
        $amount = null;
        $repaymentTimes = [];
        foreach ($this->sheet->toArray() as $row) {
            $i++;
            if ($row[0] > $this->maxFixationTime) {
                if ($i == 1) {
                    $repaymentTimes = [(int)$row[1], (int)$row[3], (int)$row[5]];
                }
                $amount = (int)$row[0];
            } elseif ($row[0] !== null) {
                $result = $this->addRow($row, $repaymentTimes, $result, $amount);
            }
        }
        return $result;
    }

    /**
     * @param $row
     * @param array $repaymentTimes
     * @param array $result
     * @param $amount
     * @return array
     */
    private function addRow($row, array $repaymentTimes, array $result, $amount): array
    {
        $fixation = (int)$row[0];
        for ($n = 0; $n < count($repaymentTimes); $n++) {
            $result[$amount][$fixation][$repaymentTimes[$n]] = ['interestRate' => $row[$n * 2 + 1], 'rpsn' => $row[$n * 2 + 2]];
        }
        return $result;
    }

}