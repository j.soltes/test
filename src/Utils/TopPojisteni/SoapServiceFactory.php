<?php


namespace App\Utils\TopPojisteni;


class SoapServiceFactory
{
    /**
     * Create TopPojisteni SOAP service
     * There is a bug on the SOAP server 'http://www.toppojisteni.net/zadani/soap/server.php?wsdl', it is necessary use non wsdl connection.
     * @return SoapService
     */
    public static function create()
    {
        return new SoapService(null, [
            'location' => 'http://www.toppojisteni.net/zadani/soap/server.php',
            'uri' => "http://www.toppojisteni.net/zadani/soap/server.php",
            'trace' => 1,
        ]);
    }
}