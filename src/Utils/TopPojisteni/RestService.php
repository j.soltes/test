<?php


namespace App\Utils\TopPojisteni;


use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use GuzzleHttp\RequestOptions;

class RestService
{

    /**
     * @var Client
     */
    private $client;

    /**
     * @var array
     */
    private $promises;

    /**
     * RestService constructor.
     * @param string|null $restServer
     */
    public function __construct(?string $restServer)
    {
        $this->client = new Client(['base_uri' => $restServer]);
    }

    public function addRequestJson(string $uri, array $data, string $key){
        $this->promises[$key] = $this->client->getAsync($uri,[RequestOptions::JSON => $data]);
    }

    public function addRequestGet(string $uri, array $data, string $key){
        $this->promises[$key] = $this->client->getAsync($uri,[RequestOptions::QUERY => $data]);
    }

    /**
     * @throws \Throwable
     */
    public function getResults(){
        $results = Promise\unwrap($this->promises);
        foreach ($results as &$result){
            $result = json_decode($result->getBody());
        }
        return  $results;
    }

    public function clear(){
        $this->promises = [];
    }


}